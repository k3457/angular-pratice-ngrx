import { Action } from "../actions";
import { Post } from "../models/posts";
import { POST_ACTION_ERROR, POST_ACTION_REQUEST, POST_ACTION_SUCCESS, POST_ADD_ACTION, POST_DELETE_ACTION, POST_UPDATE_ACTION } from "../actions/post-action";
import { StoreUtility } from "../utlis/store.utlity";
import { createSelector } from "@ngrx/store";

export interface PostReducerState {
    loading: boolean;
    loaded: boolean;
    error: boolean;
    entities: {[id: number]: Post};
    ids: number[];
}

export const intialState: PostReducerState = {
    loaded: false,
    loading: false,
    error: false,
    entities: {},
    ids: []
}

export function postReducer(state = intialState, action: Action): PostReducerState{
    switch (action.type){
        case POST_ACTION_REQUEST: {
            return {...state, loading: true}
        }
        case POST_ACTION_SUCCESS: {
            const posts = action.payload.data;
            const obj = StoreUtility.normalize(posts);
            const newEntites = {...state.entities, ...obj};
            const ids = posts.map((post: any) => post.id);
            const newIds = StoreUtility.filterDuplicateIds([...state.ids, ...ids]);
            return {...state, ...{loaded: true,loading: false, error: false, entities: newEntites, ids: newIds}};
        }
        case POST_ACTION_ERROR: {
            return {...state, error: true, loading: false}
        }
        case POST_DELETE_ACTION: {
            const id = action.payload.id; 
            const newIds = state.ids.filter(elem => elem !== id);
            const newEntites = StoreUtility.removeKey(state.entities,id);
            return {...state,...{entities: newEntites},...{ids: newIds}};
        }
        case POST_UPDATE_ACTION: {
            const post = action.payload.data;
            const entity = {[post.id]: post};
            const updatedEntites = {...state.entities, ...entity};
            return {...state,...{entities: updatedEntites}};
        }
        case POST_ADD_ACTION: {
            const post = action.payload.data;
            const entity = {[post.id]: post};
            const newEntites  = {...state.entities, ...entity};
            const newIds = StoreUtility.filterDuplicateIds([...state.ids,post.id]);
            return {...state,...{entities: newEntites, ids:newIds}};
        }
        default: {
            return state;
          }
    }
}
// Selectors

export const getLoading = (state: PostReducerState) => state.loading;
export const getLoaded = (state: PostReducerState) => state.loaded;
export const getEntites = (state: PostReducerState) => state.entities;
export const getIds = (state: PostReducerState) => state.ids;
export const getPosts = createSelector(getEntites,getIds,
    (entities) => StoreUtility.unNormalized(entities));
export const getError = (state: PostReducerState) => state.error;