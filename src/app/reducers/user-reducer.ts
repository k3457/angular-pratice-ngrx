import { createSelector } from "@ngrx/store";
import { Action } from "../actions";
import { USER_ACTION_ERROR, USER_ACTION_REQUEST, USER_ACTION_SUCCESS, USER_ADD_ACTION, USER_DELETE_ACTION, USER_UPDATE_ACTION } from "../actions/user-action";
import { User } from "../models/users";
import { StoreUtility } from "../utlis/store.utlity";


export interface UserReducerState {
    loading: boolean;
    loaded: boolean;
    error: boolean;
    entities: {[id: number]: User};
    ids: number[];
}

const intitalState: UserReducerState = {
    loaded: false,
    loading: false,
    error: false,
    entities: {},
    ids: []
}

export function userReducer(state = intitalState, action: Action): UserReducerState{
    switch (action.type){
        case USER_ACTION_REQUEST: {
            return {...state, loading: true}
        }
        case USER_ACTION_SUCCESS: {
            const users = action.payload.data;
            const obj = StoreUtility.normalize(users);
            const newEntites = {...state.entities, ...obj};
            const ids = users.map((user: any) => user.id);
            const newIds = StoreUtility.filterDuplicateIds([...state.ids, ...ids]);
            return {...state, ...{loaded: true,loading: false, error: false, entities: newEntites, ids: newIds}};
        }
        case USER_ACTION_ERROR: {
            return {...state, error: true, loading: false}
        }
        case USER_DELETE_ACTION: {
            const id = action.payload.id; 
            const newIds = state.ids.filter(elem => elem !== id);
            const newEntites = StoreUtility.removeKey(state.entities,id);
            return {...state,...{entities: newEntites},...{ids: newIds}};
        }
        case USER_UPDATE_ACTION: {
            const user = action.payload.data;
            const entity = {[user.id]: user};
            const updatedEntites = {...state.entities, ...entity};
            return {...state,...{entities: updatedEntites}};
        }
        case USER_ADD_ACTION: {
            const user = action.payload.data;
            const entity = {[user.id]: user};
            const newEntites  = {...state.entities, ...entity};
            const newIds = StoreUtility.filterDuplicateIds([...state.ids,user.id]);
            return {...state,...{entities: newEntites, ids:newIds}};
        }
        default: {
            return state;
          }
    }
}

// Selectors

export const getLoading = (state: UserReducerState) => state.loading;
export const getLoaded = (state: UserReducerState) => state.loaded;
export const getEntites = (state: UserReducerState) => state.entities;
export const getIds = (state: UserReducerState) => state.ids;
export const getUsers = createSelector(getEntites,getIds,
    (entities) => StoreUtility.unNormalized(entities));
export const getError = (state: UserReducerState) => state.error;