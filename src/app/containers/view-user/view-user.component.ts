import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { filter, map, switchMap, takeWhile } from 'rxjs';
import { User } from 'src/app/models/users';
import { RepoService } from 'src/app/services/repo.service';

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.css']
})
export class ViewUserComponent implements OnInit, OnDestroy {

  isAlive = true;
  user!: User;

  constructor(private route: ActivatedRoute, private repo: RepoService) { }

  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    const user$ = this.route.params.pipe(map(data => data['id']),
      takeWhile(() => this.isAlive),
      switchMap((id) => {
        return this.repo.getUserById(id);
      }), filter(res => !!res));
    user$.subscribe(data => {
      this.user = data;
    });
  }

  ngOnDestroy() {
    this.isAlive = false;
  }


}
