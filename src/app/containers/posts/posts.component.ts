import { Component, Input, OnInit } from '@angular/core';
import { Post } from 'src/app/models/posts';
import { User } from 'src/app/models/users';
import { RepoService } from 'src/app/services/repo.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  postList!: Post[];
  constructor(private repo: RepoService) { }

  ngOnInit(): void {

    this.fetchData();
  }

  fetchData(){
    

  }

}
