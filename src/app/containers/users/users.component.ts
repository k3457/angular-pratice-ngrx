import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { takeWhile } from 'rxjs';
import { UpdateUserComponent } from 'src/app/components/update-user/update-user.component';
import { User } from 'src/app/models/users';
import { RepoService } from 'src/app/services/repo.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit, OnDestroy {

  users: User[] = [];
  loading = false;
  error = false;
  isAlive = true;

  constructor(private repo: RepoService, private dialog: MatDialog) { }

  ngOnInit() {
    this.fetchData();
  }

  fetchData() {
    const observer$ = this.repo.getUserList()
    const loading$ = observer$[0];
    const getUserData$ = observer$[1];
    const error$ = observer$[2];
    getUserData$.pipe(takeWhile(()=> this.isAlive)).subscribe((data) => {
      this.users = data;
    })

    loading$.pipe(takeWhile(()=> this.isAlive)).subscribe(data => {
      this.loading = data;
    })

    error$.pipe(takeWhile(()=> this.isAlive)).subscribe(data => {
      this.error = data;
    })
  }

  tryAgain() {
    this.repo.getUserList(true);
  }

  adduser(){
    this.dialog.open(UpdateUserComponent,{
      width: '260px'
    })
  }

  ngOnDestroy(): void {
    this.isAlive = false;
  }
}
