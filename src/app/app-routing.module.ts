import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './components/layout/dashboard/dashboard.component';
import { UsersComponent } from './containers/users/users.component';
import { PostsComponent } from './containers/posts/posts.component';
import { ViewUserComponent } from './containers/view-user/view-user.component';

const routes: Routes = [
  {
    path: '', component: DashboardComponent,
    children:[
      {path: '', component: UsersComponent},
      {path: 'posts', component: PostsComponent},
      {path: 'user/:id', component: ViewUserComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
