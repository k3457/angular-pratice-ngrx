import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http'
import { DashboardComponent } from './components/layout/dashboard/dashboard.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { MainLayoutComponent } from './components/layout/main-layout/main-layout.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule, FlexModule } from '@angular/flex-layout';
import { MaterialModule } from './material.module';
import { UsersComponent } from './containers/users/users.component';
import { PostsComponent } from './containers/posts/posts.component';
import { HttpService } from './services/http.service';
import { ApiService } from './services/api.service';
import { UserCardComponent } from './components/users/user-card/user-card.component';
import { UserListComponent } from './components/users/user-list/user-list.component';
import { StoreModule } from '@ngrx/store';
import { rootReducer } from './reducers';
import { RepoService } from './services/repo.service';
import { ErrorComponent } from './components/error/error.component';
import { UpdateUserComponent } from './components/update-user/update-user.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ViewUserComponent } from './containers/view-user/view-user.component';
import { PostListComponent } from './components/posts/post-list/post-list.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    HeaderComponent,
    MainLayoutComponent,
    UsersComponent,
    PostsComponent,
    UserCardComponent,
    UserListComponent,
    ErrorComponent,
    UpdateUserComponent,
    ViewUserComponent,
    PostListComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    FlexModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
    StoreModule.forRoot(rootReducer)
  ],
  providers: [HttpService, ApiService, RepoService],
  bootstrap: [AppComponent]
})
export class AppModule { }
