import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { RootReducerState, getPost, getPostError, getPostLoaded, getPostLoading, getUser, getUserById, getUserError, getUserLoaded, getUserLoading } from '../reducers';
import { ApiService } from './api.service';
import { Observable, combineLatest, take } from 'rxjs';
import { UserActionAdd, UserActionDelete, UserActionRequestAction, UserActionSuccessAction, UserActionUpdate, UserErrorAction } from '../actions/user-action';
import { User } from '../models/users';
import { Post } from '../models/posts';
import { PostActionRequestAction, PostActionSuccessAction, PostErrorAction } from '../actions/post-action';

@Injectable({
  providedIn: 'root'
})
export class RepoService {

  constructor(private apiService: ApiService, private store: Store<RootReducerState>) { }

  getUserList(force = false): [Observable<boolean>, Observable<User[]>, Observable<boolean>]{
    const loading$ = this.store.select(getUserLoading);
    const loaded$ = this.store.select(getUserLoaded);
    const getUserData$ = this.store.select(getUser);
    const getError$ = this.store.select(getUserError);
    combineLatest([loading$, loaded$]).pipe(take(1)).subscribe((data) => {
      if ((!data[0] && !data[1]) || force) {
        this.store.dispatch(new UserActionRequestAction());
        this.apiService.getAllUser().subscribe(res => {
          this.store.dispatch(new UserActionSuccessAction({ data: res }));
        },error =>{
          this.store.dispatch(new UserErrorAction());
        })
      }
    });

    return [loading$,getUserData$,getError$];
  }

  deleteUser(id: number){
    this.store.dispatch(new UserActionDelete({id}));
  }

  updateUser(data: User){
      this.store.dispatch(new UserActionUpdate({data}));
  }

  addUser(data: User){
    this.store.dispatch(new UserActionAdd({data}));
  }

  getUserById(id: number, force = false){
    const users$ = this.store.select(state => getUserById(state,id));
    users$.pipe(take(1)).subscribe(res=>{
      if(res || !res) {
        this.apiService.getUser(id).subscribe(data =>{
          this.store.dispatch(new UserActionUpdate({data}));
        })  
      }
      return res;
    })

    return users$;
  }

  getAllPost(force = false): [Observable<boolean>, Observable<Post[]>, Observable<boolean>] {
    const posts$ = this.store.select(getPost);
    const loading$ = this.store.select(getPostLoading);
    const loaded$ = this.store.select(getPostLoaded);
    const getPostError$ = this.store.select(getPostError);
    combineLatest([loading$, loaded$]).pipe(take(1)).subscribe((data) => {
      if ((!data[0] && !data[1]) || force) {
        this.store.dispatch(new PostActionRequestAction());
        this.apiService.getAllPost().subscribe(res => {
          this.store.dispatch(new PostActionSuccessAction({ data: res }));
        },error =>{
          this.store.dispatch(new PostErrorAction());
        })
      }
    });
    return [loading$,posts$,getPostError$];
  }
}
