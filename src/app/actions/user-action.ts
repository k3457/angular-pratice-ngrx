import { User } from "../models/users";

export const USER_ACTION_REQUEST = "user list request";
export const USER_ACTION_SUCCESS = "user list success";
export const USER_ACTION_ERROR = "user list error";
export const USER_DELETE_ACTION = "user delete action";
export const USER_UPDATE_ACTION = "user update action";
export const USER_ADD_ACTION = "user add action";

export class UserActionRequestAction {
    readonly type = USER_ACTION_REQUEST;
}

export class UserActionSuccessAction {
    readonly type = USER_ACTION_SUCCESS;
    constructor(public payload?: {data: User[]}) { }
}

export class UserErrorAction {
    readonly type = USER_ACTION_ERROR;
}

export class UserActionDelete { 
    readonly type = USER_DELETE_ACTION;
    constructor(public payload?: {id: number}) { }
}

export class UserActionUpdate { 
    readonly type = USER_UPDATE_ACTION;
    constructor(public payload?: {data: User}) { }
}

export class UserActionAdd{
    readonly type = USER_ADD_ACTION;
    constructor(public payload?: {data: User}) { }
}
