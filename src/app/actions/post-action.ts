import { Post } from "../models/posts";

export const POST_ACTION_REQUEST = "post list request";
export const POST_ACTION_SUCCESS = "post list success";
export const POST_ACTION_ERROR = "post list error";
export const POST_DELETE_ACTION = "post delete action";
export const POST_UPDATE_ACTION = "post update action";
export const POST_ADD_ACTION = "post add action";

export class PostActionRequestAction {
    readonly type = POST_ACTION_REQUEST;
}

export class PostActionSuccessAction {
    readonly type = POST_ACTION_SUCCESS;
    constructor(public payload?: {data: Post[]}) { }
}

export class PostErrorAction {
    readonly type = POST_ACTION_ERROR;
}

export class PostActionDelete { 
    readonly type = POST_DELETE_ACTION;
    constructor(public payload?: {id: number}) { }
}

export class PostActionUpdate { 
    readonly type = POST_UPDATE_ACTION;
    constructor(public payload?: {data: Post}) { }
}

export class PostActionAdd{
    readonly type = POST_ADD_ACTION;
    constructor(public payload?: {data: Post}) { }
}

