import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { User } from 'src/app/models/users';
import { RepoService } from 'src/app/services/repo.service';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {

  userForm!: FormGroup;

  constructor(private dialogRef:MatDialogRef<UpdateUserComponent>,
    @Inject(MAT_DIALOG_DATA) public data: User, private repo: RepoService) {}

  ngOnInit(): void {

    this.userForm = new FormGroup({
      name: new FormControl(this.data ? this.data.name: null,[Validators.required]),
      email: new FormControl(this.data ? this.data.email : null,[Validators.required])
    })
  }

  addOrUpdateUser(){
    if(this.data){
      this.updateUser();
    }
    else{
      this.addUser();
    }
  }

  addUser() {
    this.repo.addUser(this.userForm.value);
    this.dialogRef.close();
  } 

  updateUser(){
      const updatedUser = {...this.data,...this.userForm.value};
      this.repo.updateUser(updatedUser);
      this.dialogRef.close();
  }

}
