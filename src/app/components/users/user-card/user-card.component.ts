import { Component, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { User } from 'src/app/models/users';
import { ApiService } from 'src/app/services/api.service';
import { RepoService } from 'src/app/services/repo.service';
import { UpdateUserComponent } from '../../update-user/update-user.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-user-card',
  templateUrl: './user-card.component.html',
  styleUrls: ['./user-card.component.css']
})
export class UserCardComponent implements OnInit {

  constructor(private repo: RepoService, private dialog: MatDialog, private router: Router) { }

  @Input() user!: User;


  ngOnInit(): void {
  }

  delete(){
    this.repo.deleteUser(this.user.id)
  }

  update(){
    this.dialog.open(UpdateUserComponent,{
      width: '260px',
      data: this.user
    })
  }

  open(){
    this.router.navigate(['user',this.user.id]);
  }

}
